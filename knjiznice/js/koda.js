/* global L, distance, CanvasJS */
var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";

var generatedEHR = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

var klik_lat = FRI_LAT;
var klik_lng = FRI_LNG;

var polygons = [];
var props = [];

/*global $*/

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  var ime, priimek, datumRojstva, masa, visina;
  generatedEHR = [];
  $("#obvGen").hide();
  $("#obvGen").html("");
  if(stPacienta == 1)
  {
    ime = "Aleks";
    priimek = "Pleško";
    datumRojstva = "1980-07-11";
    masa = 78;
    visina = 190;
  }
  else if(stPacienta == 2)
  {
    ime = "Zoran";
    priimek = "Furlan";
    datumRojstva = "1982-02-05";
    visina = 179;
    masa = 57;
  }
  else if(stPacienta == 3)
  {
    ime = "Valentin";
    priimek = "Novak";
    datumRojstva = "1983-11-05";
    visina = 175;
    masa = 105;
  }
 
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers:{
      "Authorization": getAuthorization()
    },
    success: function(data){
      var ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: {"ehrId": ehrId}
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: "POST",
        headers:{
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party){
          if(party.action == 'CREATE'){
            console.log(partyData);
            console.log(ehrId);
            //generatedEHR.push(ehrId);
            $("#obvGen").show();
            $("#obvGen").append("Generiran " + partyData.firstNames + " " + partyData.lastNames + " (" + partyData.dateOfBirth + ") z EHR ID " + ehrId + "<br>");
            if(partyData.firstNames == "Aleks")
            {
              generatedEHR[0] = ehrId;
            }
            else if(partyData.firstNames == "Zoran")
            {
              generatedEHR[1] = ehrId;
            }
            else
            {
              generatedEHR[2] = ehrId;
            }
            dodajMeritveVitalnihZnakov(ehrId,masa,visina);
            dodajMeritveVitalnihZnakov(ehrId,masa + getRandomInt(-3, 3),visina);
            dodajMeritveVitalnihZnakov(ehrId,masa + getRandomInt(-3, 3),visina);
            dodajMeritveVitalnihZnakov(ehrId,masa + getRandomInt(-3, 3),visina);
            dodajMeritveVitalnihZnakov(ehrId,masa + getRandomInt(-3, 3),visina);
          }
        },
        error: function(err)
        {
          console.log(JSON.parse(err.responseText).userMessage);
        }
      })
    }
  })
}

function preberiEHRodBolnika(EHR_ID) {
	var ehrId = EHR_ID;

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			console.log(party);
  			/*
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");*/
        $("#izpisEHR").val(party.additionalInfo.ehrId);
        $("#izpisIme").val(party.firstNames);
        $("#izpisPriimek").val(party.lastNames);
        $("#izpisDR").val(party.dateOfBirth);
        $.ajaxSetup({
            headers: {
                "Authorization": getAuthorization()
            }
        });
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/" + "weight",
            type: 'GET',
            success: function (res) {
                //var mase = [ ];
                //var datumi = [];
                console.log(res);
                console.log("test fetch 1");
                
                //graf
                var chart = new CanvasJS.Chart("chartContainer",
                {
                  title:{
                  text: "Statistika telesnih mas (kg)"
                  },
                   data: [
                  {
                    type: "line",
            
                    dataPoints: []
                  }
                  ]
                });
                
                //konca grafa
                
                //console.log(res);
                //for (var i in res) {
                for (var i = res.length-1;i >= 0;i--) {
                    //$("#izpisMasa").val(res[i].weight);
                    var arr = res[i].time.substring(0,10).split('-');
                    $("#izpisMasa").val(res[i].weight.toFixed(1));
                    chart.options.data[0].dataPoints.push({x: new Date(parseInt(arr[0]), parseInt(arr[1]), parseInt(arr[2])), y: parseInt(res[i].weight.toFixed(1))});
                    //console.log(res[i].time.substring(0,10));
                    //datumi.push(arr);
                    //mase.push(res[i].weight.toFixed(1));
                    //chart.options.data[0].dataPoints[i].x = new Date(arr[0], arr[1], arr[2]);
                    //chart.options.data[0].dataPoints[i].y = res[i].weight.toFixed(1);
                }
                chart.render();
                $.ajax({
                  url: baseUrl + "/view/" + ehrId + "/" + "height",
                  type: 'GET',
                  success: function (res) {
                      console.log("test fetch 2");
                      //console.log(res);
                      for (var i in res) {
                          //$("#izpisMasa").val(res[i].weight);
                          $("#izpisVisina").val(res[i].height);
                      }
                      izracunBMI($("#izpisVisina").val(), $("#izpisMasa").val());
                      
                  }
              });
            }
        });
  		},
  		error: function(err) {
  			console.log(JSON.parse(err.responseText).userMessage);
  		}
		});
	}
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

var mesecCounter = 1;

function dodajMeritveVitalnihZnakov(EHR_ID, MASA, VISINA) {
  
  var day = 25;
  var month = mesecCounter;
  mesecCounter++;
  if(mesecCounter == 13)
    mesecCounter = 1;
  
	var ehrId = EHR_ID;
	var datumInUra = "2019-" + zeroPad(month, 2) + "-" + zeroPad(day, 2);
	var telesnaVisina = VISINA;
	var telesnaTeza = MASA;
	var telesnaTemperatura = 1;
	var sistolicniKrvniTlak = 1;
	var diastolicniKrvniTlak = 1;
	var nasicenostKrviSKisikom = 1;
	var merilec = 1;

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        
      },
      error: function(err) {
        console.log(JSON.parse(err.responseText).userMessage)
      }
		});
	}
}

function generirajVsePodatke(){
  //console.log("called");
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);
  $("#dropdownIzbiraPacienta").attr("disabled",false);
  $("#dropdownIzbiraPacienta").html("Pritisnite za izbor generiranega pacienta");
}

function prepisPodatkov(stPacienta) {
  $("#vnosEHR").val(generatedEHR[stPacienta-1]);
  $("#obvGen").hide();
}

function izpisPodatkov() {
  preberiEHRodBolnika( $("#vnosEHR").val());
  $("#obvGen").hide();
}

function izracunBMI(visina, masa){
  var kategorija;
  console.log("Izracunanje BMI");
  visina = visina / 100;
  var rezultat = masa / (visina * visina);
  $("#izpisBMI").val(rezultat.toFixed(1));
  $("#obvBMI").show();
  if(rezultat < 16.0)
  {
    kategorija = "Huda podhranjenost";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else if(rezultat >= 16.0 && rezultat < 17.0)
  {
    kategorija = "Srednja podhranjenost";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else if(rezultat >= 17.0 && rezultat < 18.5)
  {
    kategorija = "Blaga podhranjenost";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else if(rezultat >= 18.5 && rezultat < 25.0)
  {
    kategorija = "Normalna telesna masa";
    $("#obvBMI").addClass("alert-success");
    $("#obvBMI").removeClass("alert-danger");
  }
  else if(rezultat >= 25.0 && rezultat < 30.0)
  {
    kategorija = "Prekomerna telesna teža";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else if(rezultat >= 30.0 && rezultat < 35.0)
  {
    kategorija = "Debelost stopnje I";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else if(rezultat >= 35.0 && rezultat < 40.0)
  {
    kategorija = "Debelost stopnje II";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  else
  {
    kategorija = "Debelost stopnje III";
    $("#obvBMI").removeClass("alert-success");
    $("#obvBMI").addClass("alert-danger");
  }
  $("#obvBMI").html("Kategorija: " + kategorija);
  
  if(rezultat < 18.5)
  {
    var rez = 18.5 * visina * visina - masa;
    $("#obvBMI").append("<br>Za doseg normalnega indeksa telesne teže morate pridobiti: " + rez.toFixed(1) + " kg");
  }
  else if(rezultat >= 25.0)
  {
    var rez = masa - 25.0 * visina * visina;
    $("#obvBMI").append("<br>Za doseg normalnega indeksa telesne teže morate izgubiti: " + rez.toFixed(1) + " kg");
  }
  
}

function obKlikuNaMapo(e) {
  var latlng = e.latlng;
  console.log(latlng);
  klik_lat = latlng.lat;
  klik_lng = latlng.lng;
  polygons.forEach(function(poly){
    var p_latlng = poly.getCenter();
    poly.setStyle({color: vrniBarvo(p_latlng.lng,p_latlng.lat)});
  });
}

function obKlikuNaPolygon(e) {
  var index;
  for(var i = 0; i < polygons.length;i++)
  {
    if(polygons[i] == e.target)
    {
      console.log("Found the poly");
      index = i;
      break;
    }
  }
  var latlng = e.latlng;
  var popup = L.popup();
  popup
      .setLatLng(latlng)
      .setContent(props[index].name + "<br>" +
                  props[index]["addr:street"] + ", " + props[index]["addr:city"])
      .openOn(mapa);
}

function vrniBarvo(lng, lat) {
  var radij = 2;
  if (distance(lat, lng, klik_lat, klik_lng, "K") >= radij) 
    return "blue";
  else
    return "green";
}

function izrisiPoligone() {
  var url = "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json";
  
  $.getJSON(url, function (json) {
      //console.log(json.features);
      
      json.features.forEach(function(obj) {
        for(var i = 0; i < obj.geometry.coordinates.length;i++)
        {
          for(var j = 0; j < obj.geometry.coordinates[i].length;j++)
          {
            var temp = obj.geometry.coordinates[i][j][0];
            obj.geometry.coordinates[i][j][0] = obj.geometry.coordinates[i][j][1];
            obj.geometry.coordinates[i][j][1] = temp;
          }
        }
        //console.log(obj.geometry.coordinates);
        var polygon = L.polygon(obj.geometry.coordinates, {color: vrniBarvo(obj.geometry.coordinates[0][0][1],obj.geometry.coordinates[0][0][0])}).addTo(mapa);
        polygons.push(polygon);
        props.push(obj.properties);
        polygon.on('click', obKlikuNaPolygon);
      });
  
  });
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
window.addEventListener('load', function () {
  if ($("#mapa_id").length > 0)
  {
    
    // Osnovne lastnosti mape
    var mapOptions = {
      center: [FRI_LAT, FRI_LNG],
      zoom: 12
      // maxZoom: 3
    };
  
    // Ustvarimo objekt mapa
    mapa = new L.map('mapa_id', mapOptions);
    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);
  
    mapa.on('click', obKlikuNaMapo);
    izrisiPoligone();
  }
});